#! /bin/bash

# Dans ce bloc de code on demande à l'utilisateur quelle type fichier audio
# qu'il faut convertir
liste_audio="audio.txt"
echo -n "Quelle type de fichier audio voulez-vous ? "
read ext_audio
find *.$ext_audio > "$liste_audio"
echo "Liste des fichiers $ext_audio listés dans $liste_audio"

# On vérifie l'existence du dossier
# si oui le script attend une réponse de l'utilisateur
# dans le cas contraire le dossier est créé automatiquement
dossier_parent=$(basename "$PWD")
dossier_sortie=$(tr -d " " <<< $dossier_parent)
if [ -d $dossier_sortie ]; then
    echo -n "Voulez-vous supprimer le dossier $dossier_sortie ? Oui/non. "
    read reponse
    case "$reponse" in
        oui ) rm -rf $dossier_sortie && mkdir -p $dossier_sortie && echo "Dossier $dossier_sortie recréé.";;

        non ) echo "Dossier $dossier_sortie non supprimé.";;
    esac
else
    mkdir -p $dossier_sortie
    echo "Dossier $dossier_sortie créé."
fi

# Ce block de code est le coeur de ce script.
# La variable interne du shell $IFS est redéfinie au besoin,
# éviter au script de "s'embrouiller" lors la lecture du fichier audio.txt.
# Puis convertit le(s) fichier(s) audio(s) correspondant(s).
IFS_b=$IFS
IFS=$'\n'
for audio in $(cat $liste_audio); do
    audio_sortie=${audio%%.*}".mp3"
    ffmpeg -i "$audio" -f mp3 -acodec libmp3lame -y "$audio_sortie"
    echo "Conversion de $audio en $audio_sortie fait."
    mv $audio_sortie $dossier_sortie
    echo "$audio_sortie deplacé dans le dossier $dossier_sortie"
done
IFS=$IFS_b
rm $liste_audio